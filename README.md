<div align="center">

# P2PFaaS

A Framework for FaaS load balancing  | _`stack` repository_

![License](https://img.shields.io/badge/license-GPLv3-green?style=flat)

</div>

# Introduction

This repository contains a set of scripts used for configuring the framework and performing benchmarks. In particular, the relevant subdirectories are:

- `benchmark-go/` - implements a benchmark script in Go
- `benchmark/` - old benchmark script written in Python
- `functions/` - set of functions from OpenFaaS that could have been modified
- `machines-setup/` - set of scripts for configuring the nodes and the framework


# Cite the work
 If you are using P2PFaaS in your work please cite [[1]](https://dx.doi.org/10.1109/PerComWorkshops53856.2022.9767498) or [[2]](https://dx.doi.org/10.1109/TCC.2020.2968443)

```bibtex
@article{8964273,
    author={Beraldi, Roberto and Proietti Mattia, Gabriele},
    journal={IEEE Transactions on Cloud Computing},
    title={Power of random choices made efficient for fog computing},
    year={2020},
    volume={},
    number={},
    pages={1-1},
    doi={10.1109/TCC.2020.2968443}
}
```
```bibtex
@inproceedings{2022ProiettiMattiaOnRealtime,
  author = {{Proietti Mattia}, Gabriele and Beraldi, Roberto},
  booktitle = {2022 IEEE International Conference on Pervasive Computing and Communications Workshops and other Affiliated Events (PerCom Workshops)},
  title = {On real-time scheduling in Fog computing: A Reinforcement Learning algorithm with application to smart cities},
  year = {2022},
  volume = {},
  number = {},
  pages = {187-193},
  doi = {10.1109/PerComWorkshops53856.2022.9767498}
}
```

# References

1. https://dx.doi.org/10.1109/PerComWorkshops53856.2022.9767498
2. https://dx.doi.org/10.1109/TCC.2020.2968443